package com.app.blogpost.BlogPost.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PostController {

    @GetMapping("/addPost")
    public String addPost(){
        return "add_post";
    }
}
